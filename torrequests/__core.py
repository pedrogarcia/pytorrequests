import os
import subprocess

import psutil
import requests
from requests.adapters import HTTPAdapter
from stem import Signal
from stem.control import Controller
from urllib3.util.retry import Retry

from .__util import Singleton


class ProxedHTTPRequester(metaclass=Singleton):
    def __init__(self, tor_location, privoxy_location):
        _TaskManager(tor_location, privoxy_location)
        self.__proxies = {"http": "127.0.0.1:8118", "https": "127.0.0.1:8118"}
        user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.7) Gecko/2009021910 Firefox/3.0.7'
        self.__headers = {'User-Agent': user_agent}

    def head(self, url):
        session = self.__get_new_session()
        html = session.head(url, proxies=self.__proxies, headers=self.__headers)
        session.close()
        return html

    def get(self, url):
        self.__renew_connection()
        return self.__request(url)

    def __request(self, url):
        session = self.__get_new_session()
        html = session.get(url, proxies=self.__proxies, headers=self.__headers)
        session.close()
        return html

    def __get_new_session(self):
        session = requests.Session()
        adapter = HTTPAdapter(max_retries=Retry(connect=30, backoff_factor=1))
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        return session

    def __renew_connection(self):
        with Controller.from_port(port=19051) as controller:
            controller.authenticate()
            controller.signal(Signal.NEWNYM)
            controller.close()


class _TaskManager(metaclass=Singleton):
    def __init__(self, tor_location, privoxy_location):
        self.__tor = None
        self.__privoxy = None
        self.__privoxy_executable = None
        self.__privoxy_path = None
        self.__tor_executable = None
        self.__tor_path = None
        self.set_tor_location(tor_location)
        self.set_privoxy_location(privoxy_location)
        self.init_tor()
        self.init_privoxy()


    def set_privoxy_location(self, privoxy_location):
        self.__privoxy_executable = os.path.basename(privoxy_location)
        self.__privoxy_path = os.path.dirname(os.path.abspath(privoxy_location))

    def set_tor_location(self, tor_location):
        self.__tor_executable = os.path.basename(tor_location)
        self.__tor_path = os.path.dirname(os.path.abspath(tor_location))

    def checkIfTaskRunning(self, task_name):
        task_names = set([it.name() for it in psutil.process_iter(attrs=['name'])])
        return task_name in task_names

    @property
    def is_tor_running(self):
        return self.checkIfTaskRunning("tor") or self.checkIfTaskRunning("tor.exe")

    @property
    def is_privoxy_running(self):
        return self.checkIfTaskRunning("privoxy.exe") or self.checkIfTaskRunning("privoxy")

    def init_tor(self):
        if not self.is_tor_running:
            self.start_tor()

    def start_tor(self):
        tor_cwd = self.__tor_path
        torrc = os.path.join(tor_cwd, "Data", "Tor", "torrc")
        tor_exe = self.__tor_executable
        # TODO: extend for UNIX
        self.__tor = subprocess.Popen(
            [tor_exe, "-f", torrc], shell=True, cwd=tor_cwd
        )

    def init_privoxy(self):
        if not self.is_privoxy_running:
            self.start_privoxy()

    def start_privoxy(self):
        # TODO: extend for UNIX
        startupinfo = subprocess.STARTUPINFO()
        startupinfo.dwFlags = subprocess.STARTF_USESHOWWINDOW
        startupinfo.wShowWindow = 0
        CREATE_NO_WINDOW = 0x08000000
        self.__privoxy = subprocess.Popen(
            [self.__privoxy_executable],
            cwd=self.__privoxy_path,
            startupinfo=startupinfo,
            creationflags=CREATE_NO_WINDOW,
            shell=True
        )
