# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='torrequests',
    version='0.1.0',
    description='TorRequests enables HTTP requests behind Tor.',
    long_description=readme,
    author='Pedro Garcia Freitas',
    author_email='sawp@sawp.com.br',
    url='https://bitbucket.org/kuraiev/torrequests',
    license=license,
    test_suite = 'tests',
    packages=find_packages(exclude=('tests'))
)
