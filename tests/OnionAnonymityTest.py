# -*- coding: utf-8 -*-

## USE THIS SCRIPT TO TEST WHETHER Tor IS WORKING WELL AND MACHINE IS
## HIDDEN BEHIND Onion NETWORK.

from torrequests import ProxedHTTPRequester

import ipaddress
import requests
import unittest


def is_valid_ip(ip):
    try:
        ipaddress.ip_address(ip)
    except ValueError:
        return False
    return True


class TestAnonymousRequest(unittest.TestCase):
    server = 'https://api.ipify.org'

    def setUp(self):
        print("In method", self._testMethodName)

    def test_2_is_anonimous(self):
        proxy = ProxedHTTPRequester(
            tor_location="C:\\Users\\b001296460\\thirdpartsprocs\\Tor\\tor.exe",
            privoxy_location="C:\\Users\\b001296460\\thirdpartsprocs\\privoxy-3.0.26\\privoxy.exe"
        )
        real_ip = requests.get(self.__class__.server).text
        fake_ip = proxy.get(self.__class__.server).text
        print("Real={real}\tFake={fake}".format(real=real_ip, fake=fake_ip))
        self.assertNotEqual(real_ip, fake_ip, 'Proxy+Tor')

    def test_1_is_server_responding(self):
        ip = requests.get(self.__class__.server).text
        print("{ip}".format(ip=ip))
        self.assertIsNotNone(ip)
        self.assertTrue(is_valid_ip(ip))


if __name__ == "__main__":
    unittest.main()
