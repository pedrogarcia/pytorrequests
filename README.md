Build the library:

python.exe setup.py build

Test the library:

python.exe setup.py test

If all tests are passing, install the library:

python.exe setup.py install


